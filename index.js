const http = require("http");
const express = require("express");
const cors = require("cors");
const { Server } = require("socket.io");

const app = express();
const server = http.createServer(app);
const io = new Server(server, {
  cors: {
    origin: ["http://localhost:3000"],
  },
});

app.use(express.json());

app.post("/api/musicinfo", (req, res) => {
  console.log("music info has changed", JSON.stringify(req.body));
  io.emit("musicinfo", req.body);
  res.json([]);
});

app.use(
  cors({
    origin: ["http://localhost:3000", "http://localhost:4200"],
  })
);

io.on("connection", (socket) => {
    console.log('client connected');
    socket.on("disconnect", () => {
        console.log("client disconnect");
    })
})

server.listen(8000, () => {
  console.log("server listening on port 8000");
});
